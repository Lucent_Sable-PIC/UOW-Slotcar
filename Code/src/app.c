/*
 * Elevator control program, for PIC16F1825 and XC8 compiler
 *
 * Compile with  "xc8 --chip=16F1825 -m app.c"
 *
 * Author: Shea Gosnell
 *
 * This program is a controller for a small DC elevator.
 * Developed for the ENEL417 Mechatronics paper at University of Waikato.
 *
 * April 2017
 */

#include <xc.h>
//let teh __delay functions know that the processor is running at 32MHz
#define _XTAL_FREQ 32000000


//config register values
	//use the internal oscillator
	#pragma config FOSC=INTOSC
	//disable the watchdog timer
	#pragma config WDTE=OFF
	//disable power-up timer
	#pragma config PWRTE=OFF
	//enable reset pin/button
	#pragma config MCLRE=ON
	//disable conde protection
	#pragma config CP=OFF
	//disable data memory protection
	#pragma config CPD=OFF
	//disable brown-out reset
	#pragma config BOREN=OFF
	//disable clock output
	#pragma config CLKOUTEN=OFF
	//disable fail-safe clock monitor
	#pragma config FCMEN=OFF
	//disable flash memory self write protection
	#pragma config WRT=OFF
	//enable the phase-locked-loop for 4xPLL
	#pragma config PLLEN=ON
	//enable stack overflow reset
	#pragma config STVREN=OFF
	//ignored, as brown out resets are disabled
	#pragma config BORV=HI
	//enable low-voltage programming mode
	#pragma config LVP=ON

//register bit definitions
	
//Constant definitions
#define ASCII0       48
#define BRAKE_LED    LATCbits.LATC6
#define HEADLIGHTS   LATCbits.LATC2
#define SPEED        CCPR2L
#define BRAKE        LATCbits.LATC4
#define T1ON         T1CONbits.TMR1ON = 1
#define T1OFF        T1CONbits.TMR1ON = 0
#define TRACK_CHANGE LATAbits.LATA1
#define PACKET_ERR   LATAbits.LATA0
#define LED8         LATBbits.LATB7
#define LED7         LATBbits.LATB6
#define LED6         LATBbits.LATB5

//Timing definitions
//the time at which a 0 bit becomes a 1 bit is
#define PULSE_T 700

//alias for the timer registers
volatile union PULSE_LENGTH
{
	int length;
	char bytes[2];
}pulseLength;

//variables for the interrupt, which need to be used by mainline code.
//memory to hold command bytes, corresponding to states
volatile char commandBytes[8];
volatile bit packetReady = 0;

static inline void uartTxChar(char c)
{
		while(!TXSTAbits.TRMT);
		//load a character into the TX register
		TXREG = c;
}


/*
 ***uartTx(const char* m, char n)***
 * Transmit a string on the UART TX line
 * 
 * Arguments:
 *	m: The ASCII encoded string message to send
 *	n: The number of characters to transmit
*/
static void uartTx(const char* m, char n)
{
	//if there are still characters to transmit
	while(n && *m)
	{
		//and there is space in the UART transmit buffer
		uartTxChar(*m);
		m++;
		n++;
		//if we are at the end of the message, then alert that TX is done.
	}
}

/*
 ***uartSendChar(char v)***
 * Transmits a char (as ASCII) on the UART TX line
 * 
 * Arguments:
 *	v: The char value to transmit
*/
static void uartSendChar(char v)
{
	int h = v/100;
	v-=h*100;
	int t = v/10;
	v-=t*10;
	
	h+=ASCII0;
	t+=ASCII0;
	v+=ASCII0;
	
	uartTxChar(h);
	uartTxChar(t);
	uartTxChar(v);
}

/*
 ***init***
 * Sets up all PIC registers that are used during the program
*/
static inline void init(void)
{	
	//set up the oscillator
	OSCCON = 0xF0;
	//Set all a register pins as outputs
	TRISA = 0x00;
	//set all C register pins as outputs, except 0 and 1 which are the current sense and floor select
	TRISC = 0X03;
	//set all pins as digital, except the back-emf sensing, which is analog.
	ANSELA =0x00;
	ANSELB =0x00;
	ANSELC =0x01;
	
	//set all pins directions
	TRISA = 0x0C;
	TRISB = 0x00;
	TRISC = 0x81;
	
	LATA = 0;
	LATB = 0;
	LATC = 0;
	
	//set up the PWM for the motor
	//488Hz
	//use timer 2
	TRISCbits.TRISC3 = 1;
	CCPTMRSbits.C2TSEL = 0;
	//PWM has cycle frequency of 490Hz
	T2CON = 0x0;
	PR2 = 0xFF; 
	T2CONbits.TMR2ON = 1;
	//use PWM2 (CCP2) to drive the motor
	//essentially ignore the 2 LSB
	CCP2CONbits.DC2B = 0;
	//The bootstrap circuit inverts signal.
	CCP2CONbits.CCP2M = 0x0C;
	CCP2CONbits.P2M = 00;
	TRISCbits.TRISC3 = 0;

	
	//TODO:UPDATE	
	//set up the transmitter
	//set baud rate to 9600
	BAUDCONbits.BRG16 = 1;
	SPBRGH = 0x00;
	SPBRGL = 0xCF;
	//enable the UART
	RCSTAbits.CREN = 1;
	TXSTAbits.TXEN = 1;
	TXSTAbits.SYNC = 0;
	RCSTAbits.SPEN = 1;
	
	//set up the interrupt on change
	INTCONbits.IOCIE = 1;
	//falling both edges on  switch (RA2)
	IOCANbits.IOCAN2 = 1;
	IOCAPbits.IOCAP2 = 1;
	
	//set up timer1 to time edges of the signal
	T1CON = 0x00;
	T1GCON = 0x00;
	//PIE1bits.TMR1IE = 1;
	
	T1ON;
	
	/*
	//set up the ADC
	//channel AN5
	ADCON0bits.CHS = 5;
	//right justified (10 bit)
	ADCON1bits.ADFM = 1;
	//use FOSC/32 (1us conversion clock)
	ADCON1bits.ADCS = 2;
	//ADC on
	ADCON0bits.ADON = 1;
	*/
	
	INTCONbits.GIE = 1;
	INTCONbits.PEIE = 1;
	
	asm("nop");
}

/*
 ***main()***
 * Entry point
*/
void main(void)
{
	char i = 255;
	//initialise the PIC registers
	init();
	HEADLIGHTS = 1;
	BRAKE = 0;
	BRAKE_LED = 0;


//while(1)
//{
//	uartTxChar('a');
//}
	while(1)
	{
		int j;
		//if a packet is ready
		if(packetReady)
		{
			uartTx("Packet:\r\n",9);
			//check the checksum
			char checksum = 0;
			for(j = 0; j<8;j++)
			{
				checksum ^=commandBytes[j];
				uartTxChar('\t');
				uartSendChar(j);
				uartTxChar(':');
				uartSendChar(commandBytes[j]);
				uartTx("\r\n",2);
			}
			uartTx("Checksum:",9);
			uartSendChar(checksum);
			uartTx("\r\n",2);
			if(checksum == 255)
			{
				PACKET_ERR = 0;
				//valid packet, grab the data for this car
				char data = commandBytes[1];
				//get the track change bit out of the packet
				if(data & 0x40)
				{
					TRACK_CHANGE = 1;
				}
				else
				{
					TRACK_CHANGE = 0;
				}
				//get the brake bit from the packet
				if(data & 0x80)
				{
					//handle brake
					SPEED = 255;
					__delay_ms(1);
					BRAKE = 1;
					BRAKE_LED = 1;
				}
				else
				{
					BRAKE = 0;
					BRAKE_LED = 0;
					//get the motor speed from the packet
					SPEED = ((~(data & 0x3F))<<2);
					//handle motor speed	
				}
			}
			else
			{
				//invalid packet, indicate error
				PACKET_ERR = 1;
			}
			
			for(j=0;j<8;j++)
			{
				commandBytes[j] = 0x00;
			}
			packetReady = 0;
		}
		
		/*
		i-=5;
		
		if(i<20)i=255;
		
		if(i==70)
		{
			SPEED = 255;
			__delay_ms(1);
			BRAKE = 1;
			//BRAKE_LED = 1;
			__delay_ms(100);
		}
		else
		{
			BRAKE = 0;
			SPEED = i;
			//BRAKE_LED = 0;
		}

		__delay_ms(100);
		*/
	}
}


//the potential bit, 
volatile bit prevHalfBit = 0;
//state variable, processing second half of bit.
volatile bit secondEdge = 0;

//states for the command recieve state machine
volatile  signed char state = -1;
//preamble count
volatile char preCount = 0;
volatile bit awaitZero = 0;
volatile char bitWriteCount = 0;
volatile bit currentHalfBit;
//the interrupt vector, this chip has only one.
void interrupt ISR()
{
	LED8 = 0;

	if(IOCAFbits.IOCAF2)
	{
		//disable the timer to take a reading
		T1OFF;
		//clear the flag
		IOCAF = 0;
		//read the port
		PORTA;
		
		//read the time from the timer
		pulseLength.bytes[1] = TMR1H;
		pulseLength.bytes[0] = TMR1H;
		TMR1H = 0;
		TMR1L = 0;
		T1ON;
		if(packetReady)goto end_data;
		
		currentHalfBit = (pulseLength.length < PULSE_T);
		
		//on the first edge, record the time period
		if(!secondEdge)
		{
			prevHalfBit = currentHalfBit;
			secondEdge = 1;
			goto end_data;
		}
		//on second edge, compare and determine if the bit is valid
		else
		{
			//valid full bit?
			if(currentHalfBit == prevHalfBit)
			{
				secondEdge = 0;
				LED6 = currentHalfBit;
			}
			else
			{
				prevHalfBit = currentHalfBit;
				goto end_data;
			}
		}
		
		//at this point we are only executing if we have a valid bit
		//if we are waiting for a zero bit
		if(awaitZero)
		{
			//check if the current bit is zero.
			if(!currentHalfBit)
			{
				//if it is, move to the next state
				awaitZero = 0;
				bitWriteCount = 0;
				LED8 = 1;
			}
		}
		//now process the preamble
		else if(state == -1)
		{
			//if we have a one bit
			if(currentHalfBit)
			{
				//increment the preamble counter
				preCount++;
				
				//if the preamble counter reaches 13 (end of preamble)
				if(preCount == 13)
				{
					//move to next state
					state=0;
					preCount = 0;
					awaitZero = 1;
				}
			}
			else
			{
				preCount = 0;
			}
		}
		else
		{
			//in a byte-reading state
			//left shift the byte that we are currently writing
			commandBytes[state] = (commandBytes[state] << 1);
			//record the current bit
			commandBytes[state] += currentHalfBit;
			bitWriteCount++;
			
			//if we have written 8 bits, then get ready for the next byte
			if(bitWriteCount==8)
			{
				state++;
				//if we are at the last byte, then get ready for preamble
				if(state == 8)
				{
					packetReady = 1;
					state = -1;
				}
				//otherwise, await the zero at the end of the packet
				else
				{
					awaitZero = 1;
				}
			}
		}
		
	}
	
end_data:;
}